<?php

namespace App\Http\Requests;
use App\Http\Request\Request;
use Illuminate\Foundation\Http\FormRequest;

class UserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'typeDocument'=>'required',
            'document'=>'required',
            'name'=>'required',
            'lastName'=>'required',
            'password'=>'required',
            'repeat'=>'required',
            'position'=>'required',
        ];
    }
}
