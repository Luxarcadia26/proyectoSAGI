<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use App\HTTP\Requests\ProductsCreateRequest;
use App\HTTP\Requests\ProductsUpdateRequest;
use App\Products;
use Session;
use Redirect; 
use Illuminate\Http\Request;

class ProductosController extends Controller
{
    public function index(){
        return view('operario.index');
    }
    public function create(){
        return view('admin.crearProductos');
    }
    public function store(ProductsCreateRequest $request){
        \App\Products::create([
        'reference'=>$request['reference'],
        'brand'=>$request['brand'],
        'model'=>$request['model'],
        'type'=>$request['type'],
        'quantity'=>$request['quantity'],
        'color'=>$request['color'],
        'entryDate'=>$request['entryDate'],
        'importedFrom'=>$request['importedFrom'],
        ]);
        return redirect('/product/show')->with('message','Prducto Añadido Correctamente');
    }
    public function show(){
        $products = \App\Products::all();
        return view('operario.consultarProductos',compact('products'));

    }

    public function edit($id){
        $products = \App\Products::find($id);
        return view('operario.editarProductos',['products'=>$products]);
    
        }

        public function update($id,ProductsUpdateRequest $request){
            $products = \App\Products::find($id);
            $products->fill($request->all());
            $products->save();
            Session::flash('message','Datos Editados correctamente');
            return Redirect::to('/product/show');
          }
          
        }

