<table class="table table-hover">
    <thead>
    <th>Tipo Documento</th>
    <th>Documento</th>
    <th>Nombre</th>
    <th>Apellido</th>
    <th>Cargo</th>
    <th>Operaciones</th>
    </thead>
    @foreach($users as $user)
    <tbody>
    <td>{{$user->typeDocument}} </td>
    <td>{{$user->document}} </td>
    <td>{{$user->name}} </td>
    <td>{{$user->lastName}}</td>
    <td>{{$user->position}}</td>
    <td>{!! link_to_route('admin.edit', $title = 'editar', $parameters = $user->id, $attributes = ['class'=>'btn btn-success']) !!}</td>
    </tbody>
    @endforeach
    </table>
    {{ $users->links() }}