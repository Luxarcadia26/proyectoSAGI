<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use Auth;
use Session;
use Redirect;
use App\HTTP\Requests\UserLogRequest;
use Illuminate\Http\Request;

class LogController extends Controller
{
    public function index(){
        return view('login');
    }

    public function logout(){
        Auth::logout();
        return Redirect::to('/');
    }
    public function store( UserLogRequest $request){
       if (Auth::attempt(['document' => $request['document'],'password'=>$request['password'],'position'=>'Administrador']))
        {
            return Redirect::to('admin');

        }else if(Auth::attempt(['document' => $request['document'],'password'=>$request['password'],'position'=>'Operario'])){
            return Redirect::to('product');
        }else{ 
            Session::flash('message-error', "datos son Incorrectos");
            return Redirect::to('login'); 
        }
    }

        public function show(){

        }
    
    }


