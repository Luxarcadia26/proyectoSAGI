<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="img/flavicon.ico"/>
    <title>Bienvenido a SAGI</title>
    <!-- Bootstrap core CSS-->
    {!!Html::style('vendor/bootstrap/css/bootstrap.min.css')!!}
    {!!Html::style('vendor/font-awesome/css/font-awesome.min.css')!!}
    {!!Html::style('css/sb-admin.css')!!}
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
       
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive"
            aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
                  
                    <li class="nav-item" data-toggle="tooltip" data-placement="right">
                        {{HTML::image('img/logoSagi.png')}}
                    </li>

                    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Inicio">
                        <p class="nav-link">
                            <i class="fa fa-fw fa-user"></i>
                            {!!Auth::user()->name!!}
                            {!!Auth::user()->lastName!!}
                            <br>
                            {!!Auth::user()->position!!} 
                        </p>
                    </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Inicio">
                        <a class="nav-link" href="{!!URL::to('/admin')!!}">
                        <i class="fa fa-fw fa-dashboard"></i>
                        <span class="nav-link-text">Inicio</span>
                    </a>
                </li>
                
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Añadir">
                    <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
                        <i class="fa fa-fw fa-plus-circle"></i>
                        <span class="nav-link-text">Añadir</span>
                    </a>
                    <ul class="sidenav-second-level collapse" id="collapseComponents">
                            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Añada Usuarios">
                                    <a class="nav-link" href="{!!URL::to('/admin/create')!!}"><i class="fa fa-fw fa-user"></i>
                                        <span class="nav-link-text">Usuarios</span>
                                    </a>
                                </li>
                                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Añada Productos">
                                        <a class="nav-link" href="{!!URL::to('/product/create')!!}">
                                            <i class="fa fa-fw fa-th-large"></i>
                                            <span class="nav-link-text">Productos</span>
                                        </a>
                                    </li>
                    </ul>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Consultar">
                        <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti" data-parent="#exampleAccordion">
                          <i class="fa fa-fw fa-folder-open"></i>
                          <span class="nav-link-text">Consultar</span>
                        </a>
                        <ul class="sidenav-second-level collapse" id="collapseMulti">
                                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Consulte usuarios">
                                        <a class="nav-link" href="{!!URL::to('/admin/show')!!}">
                                            <i class="fa fa-fw fa-user"></i>
                                            <span class="nav-link-text">Usuarios</span>
                                        </a>
                                    </li>
                        </ul>
                      </li>
                
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Reportes">
                    <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseExamplePages" data-parent="#exampleAccordion">
                        <i class="fa fa-fw fa-file"></i>
                        <span class="nav-link-text">Reportes</span>
                    </a>
                    <ul class="sidenav-second-level collapse" id="collapseExamplePages">
                        <li>
                                <a class="nav-link" href="{!!URL::to('/reportes/error')!!}">
                                        <i class="fa fa-fw fa-inbox"></i>
                                        <span class="nav-link-text">Crear Reportes</span>
                                    </a>
                        </li>
                        <li>
                                <a class="nav-link" href="{!!URL::to('/reportes/error')!!}">
                                        <i class="fa fa-fw fa-cogs"></i>
                                        <span class="nav-link-text">Gestionar Reportes</span>
                                    </a>
                        </li>
                        <li>
                                <a class="nav-link" href="{!!URL::to('/reportes/error')!!}">
                                        <i class="fa fa-fw fa-toggle-on"></i>
                                        <span class="nav-link-text">Desabilitar Reportes</span>
                                    </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                        <a class="nav-link" href="{!!URL::to('/logout')!!}">
                            <i class="fa fa-fw fa-sign-out"></i>Logout</a>
                    </li>
            </ul>
        </div>
    </nav>
    <div class="content-wrapper">
        <!-- /.content-wrapper-->
        <div class="content-wrapper"  style="margin-left:3%;">
          @yield('contenido')
        </div>
        <footer style="background-color:#e9ecef;">
            <div class="container">
                <div class="col-md-12" style="display:inline-block;">
                    <div class="row">
                        <div class="col-4">
                                {{HTML::image('img/logoUnadGris.png')}}
                                
                        </div>
                        <div class="col-4">
                                
                                <p class="text-center"><small>Universidad Nacional Abierta y a Distancia </small></p>
                                <p class="text-center"><small>Copyright © SAGI 2018</small></p>
                                
                                
                        </div>
                        <div class="col-4">
                                {{HTML::image('img/logo-SAGI-gris.png')}}
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fa fa-angle-up"></i>
        </a>
        <!-- Logout Modal-->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-primary" href="login.html">Logout</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript-->
        {!!Html::script("vendor/jquery/jquery.min.js")!!} 
        {!!Html::script("vendor/bootstrap/js/bootstrap.bundle.min.js")!!}
        <!-- Core plugin JavaScript-->
        {!!Html::script("vendor/jquery-easing/jquery.easing.min.js")!!}
        <!-- Custom scripts for all pages-->
        {!!Html::script("js/sb-admin.js")!!}
        <!-- Custom scripts for this page-->
        <!-- Toggle between fixed and static navbar-->
        <script>
            $('#toggleNavPosition').click(function () {
                $('body').toggleClass('fixed-nav');
                $('nav').toggleClass('fixed-top static-top');
            });

        </script>
        <!-- Toggle between dark and light navbar-->
        <script>
            $('#toggleNavColor').click(function () {
                $('nav').toggleClass('navbar-dark navbar-light');
                $('nav').toggleClass('bg-dark bg-light');
                $('body').toggleClass('bg-dark bg-light');
            });

        </script>
    </div>
</body>

</html>
