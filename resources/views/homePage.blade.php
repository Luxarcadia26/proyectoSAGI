<!DOCTYPE html>
<html lang="es">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Sistema de información gestión de inventarios.">
  <meta name="keywords" content="Gestión,Inventarios">
  <meta name="author" content="Leonardo Franco">
  <link rel="shortcut icon" href="img/flavicon.ico"/>
  <title>Bienevenido SAGI</title>
  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="css/bootstrap/bootstrap.css" rel="stylesheet">


  <!-- Custom fonts for this template -->
  <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/new-age.css" rel="stylesheet">
</head>

<body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger row" href="#page-top">{!!Html::image('img/logoSagi.png')!!}</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive"
        aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#contenido">Contenido</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#cta">Equipo desarrollador</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="{!!URL::to('login')!!}">Inicia Sesión</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <header class="masthead">
    <div class="container h-100">
      <div class="row h-100">
        <div class="col-lg-7 my-auto">
          <div class="header-content mx-auto">
            <h1 class="mb-5">¿Problemas manejando el inventario de su empresa?</h1>
            <h2 class="mb-5">¡SAGI es para usted!</h2>
            <a href="#contenido" class="btn btn-outline btn-xl js-scroll-trigger">Inicia desde ahora.</a>
          </div>
        </div>
        <div class="col-lg-5 my-auto">
          <div class="device-container">
            <div class="device">
              <div class="screen">
                <!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! -->
                <img src="img/laptop2.png" class="img-fluid" alt="">
              </div>
              <div class="button">
                <!-- You can hook the "home button" to some JavaScript events or just remove it -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!--seccion contenido-->

  <section class="features" id="contenido">
    <div class="container">
      <div class="section-heading text-center">
        <h2>Administre el inventario</h2>
        <p class="text-muted">de su empresa de manera sencilla</p>
        <hr>
      </div>
      <div class="row">
        <div class="col-lg-4 my-auto">
          <div class="device-container">
            <div class="">
              <div class="device">
                <div class="screen">
                  <!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! -->
                  <img src="img/laptop1.png" class="img-fluid" alt="">
                </div>
                <div class="button">
                  <!-- You can hook the "home button" to some JavaScript events or just remove it -->
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-8 my-auto">
          <div class="container-fluid">
            <div class="row">
              <div class="col-lg-6">
                  <div class="feature-item">
                    <i class="fa fa-group text-primary" aria-hidden="true"></i>
                    <h3>Añada Usuarios</h3>
                    <p class="text-muted">SAGI permite añadir usuarios</p>
                    <p class="text-muted">con respectivos privilegios para una buena distribución de tareas.</p>
                  </div>
                </a>
              </div>
              <div class="col-lg-6">
                  <div class="feature-item">
                    <i class="fa fa-th-large text-primary" aria-hidden="true"></i>
                    <h3>Gestión de invetarios</h3>
                    <p class="text-muted">SAGI permite desde añadir, modificar y consultar productos para llevar un inventario actualizado y preciso.</p>
                  </div>
                </a>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6">
                  <div class="feature-item">
                    <i class="fa fa-area-chart text-primary" aria-hidden="true"></i>
                    <h3>Toma de descisiones</h3>
                    <p class="text-muted">SAGI le ayudará a su empresa a tomar mejores decisiones en cuanto a gestión de inventario de su empresa.</p>
                  </div>
                </a>
              </div>
              <div class="col-lg-6">
                  <div class="feature-item">
                    <i class="fa fa-inbox text-primary" aria-hidden="true"></i>
                    <h3>Reportes</h3>
                    <p class="text-muted">SAGI le permitirá generar reportes de los inventarios de su empresa.</p>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- otras seccion-->

  <section class="cta" id="cta">
    <div class="cta-content">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-3 text-center">
            <h2>Equipo desarrollo</h2>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-4">
            <div class="team-member">
              <img src="img/team/1.jpg" class="img-responsive img-circle" alt="">
              <h4 class="text-muted">Ever Leonardo Franco Rodriguez</h4>
              <h3 class="text-muted">Líder proyecto y desarrollador</h3>
              <ul class="list-inline social-buttons">
                <li><a href="https://twitter.com/leonardo_1999"><i class="fa fa-twitter"></i></a>
                </li>
                <li><a href="https://www.facebook.com/everleonardo.francorodriguez"><i class="fa fa-facebook"></i></a>
                </li>
                <li><a href="https://github.com/elfranco6"><i class="fa fa-github"></i></a>
                </li> 
              </ul>
            </div>
          </div>
            <div class="col-md-6 degrade"style="margin-bottom:25%; margin-top:3%;">
              <p class="text-muted text">Estudiante de sexto semestre de ingenieria de sistemas</p>
              <p class="text-muted text">de la Universidad Abierta y a Distancia UNAD sede Jose Acevedo y Gómez</p>
              <p class="text-muted text" style"float:left;">"Pequeños detalles, pueden llegar a sorprender"</p>
          
          </div>
        </div>
      </div>
      </div>
    </div>
  </section>


  <!-- Footer -->
  <footer class="text-center">
    <div class="footer-above">
      <div class="container">
        <div class="row">
          <div class="footer-col col-md-4">
            <img src="img/logo-SAGI-gris.png" alt="Este recurso no es soportado por el navegador actual :(" class="img-resposive">
          </div>
          <div class="footer-col col-md-4">
            <h3>Síguenos en</h3>
            <ul class="list-inline" style="margin-top:8%;">
              <li class="list-inline-item">
                <a class="btn-social btn-outline" href="https://www.facebook.com/UniversidadUNAD/">
                  <i class="fa fa-fw fa-facebook"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a class="btn-social btn-outline" href="https://twitter.com/universidadunad?lang=es">
                  <i class="fa fa-fw fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a class="btn-social btn-outline" href="https://www.unad.edu.co">
                  <i class="fa fa-fw fa-dribbble"></i>
                </a>
              </li>
              <li class="list-inline-item">
                  <a class="btn-social btn-outline" href="https://www.instagram.com/universidadunad/">
                    <i class="fa fa-fw fa-instagram"></i>
                  </a>
            </ul>
          </div>
          <div class="footer-col col-md-4">
            <img src="img/logoUnadGris.png" alt="Este recurso no es soportado por el navegador actual :(" class="img-resposive">
          </div>
        </div>
      </div>
    </div>
    <div class="footer-below">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h6>UNAD Sede nacional José Celestino Mutis: Calle 14 sur No. 14 - 23</h6>
            <h6>PBX:(+57) 1 344 3700 Bogotá D.C., Colombia</h6>
            <h6>Línea nacional gratuita desde Colombia: 018000115223</h6>
            <h6>Atención al usuario: atencionalusuario@unad.edu.co</h6>

            <p>Copyright &copy; SAGI 2018 Reservados todos los derechos.</p>
          </div>
        </div>
      </div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="js/jquery/jquery.min.js"></script>
  <script src="js/popper/popper.min.js"></script>
  <script src="js/bootstrap/bootstrap.min.js"></script>

  <!-- Custom scripts  this template -->
  <script src="js/new-age.min.js"></script>

</body>
</html>
