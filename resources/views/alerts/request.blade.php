@if(count($errors)> 0)
<div class="alert alert-danger" role="alert">
    <button type="button" class="close" data-dismiss="alert" arial-label="close"><span aria-hidden="true">&times;</span></button>
    <ul>
        @foreach($errors->all() as $error)
        <li style="text-decoration:none;">
              {!!$error!!}
        </li>
  
        @endforeach
    </ul>
  </div>
 
@endif