<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Error</title>
    {!!Html::style('vendor/bootstrap/css/bootstrap.min.css')!!}
    {!!Html::style('vendor/font-awesome/css/font-awesome.min.css')!!}
    {!!Html::style('https://fonts.googleapis.com/css?family=Poppins|Advent+Pro')!!}
    {!!Html::style('css/sb-admin.css')!!}
</head>
<body>
<div class="col-md-12">
    <div class="row">
            <p class="text-center" style="  font-family: 'Poppins', sans-serif; margin:1% 1% 2% 42%; font-size:70px; color:darkgray;">Wooops</p>
        </div>        
            <div class="row">
               {!!Html::image('img/map.png',"Imagen no encontrada", array('id' => 'principito', 'title' => 'El principito','style'=>'margin:0% 0% 0% 39%;'))!!}
            </div>
            <div class="row">
                <h1 style="  font-family: 'Advent Pro', sans-serif; margin:2% 1% 3% 30%; color:darkgray; ">No encontramos la pagina que buscas :-(</h1>
            </div>
            
           
    
</div>
</body>
</html>