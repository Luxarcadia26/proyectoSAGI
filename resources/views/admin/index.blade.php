@extends('plantillas.layout')
@section('contenido')
<div class="container-fluid">
        <h1 class="title">Inicio</h1>
        <hr> 
      </div>
      <!-- /.container-fluid-->
      <div class="content-fluid">
      </div>
<div class="container">
    <div class="col-md-12">
        <h2>Bienvenido a SAGI</h2>
    </div>
    <div class="col-md-12">
        <div class="row">
                <div class="col-md-6">
                        <i class="fa fa-fw fa-group"style="font-size:140px; margin:5% 1% 3% 30%;"></i>
                        <h4 class="text-center">Añadir Usuarios</h4>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Blanditiis debitis suscipit voluptatem dolor explicabo quas nostrum numquam nihil inventore eos asperiores dolorem est maxime ducimus autem, reprehenderit perspiciatis? Nemo, velit?</p>
                       
                </div>
                <div class="col-md-6">
                        <i class="fa fa-fw fa-th-large"style="font-size:140px; margin:5% 1% 3% 30%;"></i>
                        <h4 class="text-center">Añadir Productos</h4>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Blanditiis debitis suscipit voluptatem dolor explicabo quas nostrum numquam nihil inventore eos asperiores dolorem est maxime ducimus autem, reprehenderit perspiciatis? Nemo, velit?</p>
                </div>
        </div>
        <div class="row">
                <div class="col-md-6">
                        <i class="fa fa-fw fa-area-chart"style="font-size:140px; margin:5% 1% 3% 30%;"></i>
                        <h4 class="text-center">Consultar</h4>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Blanditiis debitis suscipit voluptatem dolor explicabo quas nostrum numquam nihil inventore eos asperiores dolorem est maxime ducimus autem, reprehenderit perspiciatis? Nemo, velit?</p>
                </div>
                <div class="col-md-6">
                        <i class="fa fa-fw fa-area-chart"style="font-size:140px; margin:5% 1% 3% 30%;"></i>
                        <h4 class="text-center">Reportes</h4>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Blanditiis debitis suscipit voluptatem dolor explicabo quas nostrum numquam nihil inventore eos asperiores dolorem est maxime ducimus autem, reprehenderit perspiciatis? Nemo, velit?</p>
                </div>
        </div>
        


    </div>
</div>
@stop