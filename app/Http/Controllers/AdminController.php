<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use App\HTTP\Requests\UserCreateRequest;
use App\HTTP\Requests\UserUpdateRequest;
use App\User;
use App\Products;
use Session;
use Redirect; 
use Illuminate\Http\Request;

class AdminController extends Controller
{  
    // metodo index Administrador
    public function index(){
        //Retorna a vista index blade
        return view('admin.index');
    }
    //metodo crear usuarios
    public function create(){
        return view('admin.createUser');
    }

    //Metodo para almacenar datos
    public function store(UserCreateRequest $request){
        \App\User::create([
        'typeDocument'=>$request['typeDocument'],
        'document'=>$request['document'],
        'name'=>$request['name'],
        'lastName'=>$request['lastName'],
        'password'=>$request['password'],
        'position'=>$request['position'],
        ]);
        return redirect('/admin/show')->with('message','Usuario Añadido Correctamente');
    }

    //metodo que muestra los usuarios creados
    public function show(){
        $users = \App\User::paginate(5);
        return view('admin.consultarUsuarios',compact('users'));
    }

    // metodo para edita datos 
    public function edit($id){
        $users = User::find($id);
        return view('admin.edit',['users'=>$users]);
    }

    // metodo para actualizar los datos 
    public function update($id, UserUpdateRequest $request){
        $users= User::find($id);
        $users->fill($request->all());
        $users->save();
        Session::flash('message','Usuario Editado correctamente');
        return Redirect::to('/admin/show');
    }

}
