<div class="row">
    <div class="col-md-6">
        {!! Form::label('reference','Referencia') !!}
        {!! Form::text('reference',null, ['class'=>'form-control']) !!}      
    </div>
    <div class="col-md-6">
        {!! Form::label('brand','Marca') !!}
        {!! Form::text('brand',null,['class'=>'form-control']) !!} 
    </div>
</div>

<div class="row">
        <div class="col-md-6">
            {!! Form::label('model','Modelo') !!}
            {!! Form::text('model',null, ['class'=>'form-control']) !!}      
        </div>
        <div class="col-md-6">
            {!! Form::label('type','Tipo') !!}
            {!! Form::text('type',null,['class'=>'form-control']) !!} 
        </div>
</div>

<div class="row">
        <div class="col-md-6">
            {!! Form::label('quantity','Cantidad') !!}
            {!! Form::text('quantity',null, ['class'=>'form-control']) !!}      
        </div>
        <div class="col-md-6">
            {!! Form::label('color','Color') !!}
            {!! Form::text('color',null,['class'=>'form-control']) !!} 
        </div>
</div>
<div class="row">
        <div class="col-md-6">
            {!! Form::label('entryDate','Fecha Ingreso') !!}
            {!! Form::text('entryDate',null, ['class'=>'form-control']) !!}      
        </div>
        <div class="col-md-6">
            {!! Form::label('importedFrom','Importado de') !!}
            {!! Form::text('importedFrom',null,['class'=>'form-control']) !!} 
        </div>
</div>
