@extends('plantillas.layout')
@section('contenido')
<div class="container-fluid">
    <h1>Añadir Productos</h1>
    <hr> 
  </div>
  <!-- /.container-fluid-->
  <div class="content-fluid">
      @include('alerts.request')
  </div>
<div class="container">
    
        {!!Form::open(['route'=>'product.store','method'=>'POST'])!!}  
        @include('formsProductos.createProductos')
    
       
</div>

<div class="col-md-12">
{!! Form::submit('Añadir Producto', ['class'=>'btn btn-primary','style'=>'margin-top:2%; margin-left:45%;']) !!}
</div>

{!! Form::close() !!}

@stop