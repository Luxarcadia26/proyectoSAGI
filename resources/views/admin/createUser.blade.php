@extends('plantillas.layout')
@section('contenido')
@include('alerts.request')
<div class="container-fluid">
    <h1>Añadir usuarios</h1>
    <hr> 
  </div>
  <!-- /.container-fluid-->
  <div class="content-fluid">
  </div>
<div class="container">
    
        {!!Form::open(['route'=>'admin.store','method'=>'POST'])!!}
        @include('formsAdmin.createUser')
   
</div>

<div class="col-md-12">
{!! Form::submit('Añadir Usuario', ['class'=>'btn btn-primary','style'=>'margin-top:2%; margin-left:45%;']) !!}
</div>

{!! Form::close() !!}

@stop