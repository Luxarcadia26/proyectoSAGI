<table class="table table-hover">
    <thead>
    <th>Referencia</th>
    <th>Marca</th>
    <th>Modelo</th>
    <th>Tipo</th>
    <th>Cantidad</th>
    <th>Color</th>
    <th>Fecha Ingreso</th>
    <th>Importado de</th>
    <th>Operaciones</th>
    </thead>
    @foreach($products as $product)
    <tbody>
        <td>{{$product->reference}} </td>
        <td>{{$product->brand}} </td>
        <td>{{$product->model}} </td>
        <td>{{$product->type}} </td>
        <td>{{$product->quantity}} </td>
        <td>{{$product->color}} </td>
        <td>{{$product->entryDate}} </td>
        <td>{{$product->importedFrom}}</td>
        <td>  {!! link_to_route('product.edit', $title = 'editar', $parameters = $product->id, $attributes = ['class'=>'btn btn-warning']) !!}</td>
    </tbody>
    @endforeach
    </table>