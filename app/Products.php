<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $fillable = [
        'reference','brand','model','type', 'quantity','color','entryDate','importedFrom'
    ];
}
