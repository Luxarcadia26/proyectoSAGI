@if(Session::has('message'))
<div class="alert alert-successs" role="alert">
    <button type="button" class="close" data-dismiss="alert" arial-label="close"><span aria-hidden="true">&times;</span></button>
</div>
{{Session::get('message')}}

@endif