<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login</title>
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" >
{!!Html::style('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css')!!}
{!!Html::style('css/estilos.css')!!}
{!!Html::style('https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300')!!}
</head>

<body>
<div id="contenedor" class="degrade" >
  <div id="logo">{!!Html::image('img/logoSagi.png')!!}</div>
	@include('alerts.errors')
<div class="ContentForm">
		 	
			 {!! Form::open(['route'=>'login.store','method'=>'POST']) !!}
			 
		 		<div class="input-group input-group-lg">
					<span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-user"></i></span>
					{!! Form::text('document',null, ['class'=>'form-control','placeholder'=>'Usuario','aria-describedby'=>'sizing-addon1']) !!}
				</div>
				<br>
				<div class="input-group input-group-lg">
				  <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-lock"></i></span>
					{!! Form::password('password', ['class'=>'form-control','placeholder'=>'*******','aria-describedby'=>'sizing-addon1']) !!}
				</div>
				<br>
				<button class="btn btn-lg btn-primary btn-block btn-signin" id="IngresoLog" type="submit">Iniciar Sesión</button>
		 	
			 {!! Form::close() !!}
			 @include('alerts.request')
		 </div>	
</div>
</body>
</html>
