@extends('plantillas.layout')
@section('contenido')
@include('alerts.request')
<div class="container-fluid">
    <h1>Editar usuarios</h1>
    <hr> 
  </div>
  <!-- /.container-fluid-->
  <div class="content-fluid">
  </div>
  <div class="container">
        {!!Form::model($users, ['route' => ['admin.update', $users->id],'method'=>'PUT'])!!}
        @include('formsAdmin.edit')
       
</div>
<div class="col-md-12">
{!! Form::submit('Añadir Usuario', ['class'=>'btn btn-primary','style'=>'margin-top:2%; margin-left:45%;']) !!}
</div>

{!! Form::close() !!}
@stop