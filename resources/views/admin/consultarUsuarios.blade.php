@extends('plantillas.layout')

@section('contenido')
<div class="container-fluid">
    @if(Session::has('message'))
    <div class="alert alert-success" role="alert">
        <button type="button" class="close" data-dismiss="alert" arial-label="close"><span aria-hidden="true">&times;</span></button>
      {{Session::get('message')}}
      </div>
    @endif
    <h1>Consultar Usuarios</h1>
    <hr> 
  </div>
  <!-- /.container-fluid-->
  <div class="content-fluid">
  </div>
  @include('formsAdmin.consultarUsuarios')
@stop