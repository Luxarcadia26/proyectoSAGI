<div class="row">
    <div class="col-md-6">
        {!! Form::label('typeDocument','Tipo Documento') !!}   
        {!! Form::select('typeDocument',[
            'documents' =>['Seleccione...','Cedula','pasaporte Extranjería']
        ],'Seleccione...', ['class'=>'form-control']) !!}
        
    </div>
    <div class="col-md-6">
        {!! Form::label('document','Numero Documento') !!}
        {!! Form::text('document',null,['class'=>'form-control']) !!} 
    </div>
</div>
<div class="row">
<div class="col-md-6">
{!! Form::label('name','Nombre') !!}
{!! Form::text('name',null,['class'=>'form-control']) !!}
</div>
<div class="col-md-6">
{!! Form::label('lastName','Apellido') !!}
{!! Form::text('lastName',null,['class'=>'form-control']) !!} 
</div>
</div>
<div class="row">
<div class="col-md-4">
{!! Form::label('password','Contraseña') !!}
{!!Form::password('password', ['class' => 'form-control']) !!}
</div>
<div class="col-md-4">
{!! Form::label('repeat','Repetir contraseña') !!} 
{!!Form::password('repeat', ['class' => 'form-control']) !!}
</div> 
<div class="col-md-4">
{!! Form::label('position','Cargo') !!} 
{!! Form::text('position',null,['class'=>'form-control']) !!}
</div> 
</div> 