@extends('plantillas.layoutOperario')
@section('contenido')
<div class="container-fluid">
    <h1>Añadir Productos</h1>
    <hr> 
  </div>
  <!-- /.container-fluid-->
  <div class="content-fluid">
        @include('alerts.request')
  </div>
<div class="container">
    
    {!!Form::model($products, ['route' => ['product.update', $products->id],'method'=>'PUT'])!!}
    @include('formsProductos.editarProductos')
        
</div>

<div class="col-md-12">
{!! Form::submit('Modificar Producto', ['class'=>'btn btn-primary','style'=>'margin-top:2%; margin-left:45%;']) !!}
</div>

{!! Form::close() !!}

@stop